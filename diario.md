# Diario lezioni

## Lezioni (in ordine cronologico inverso)

* data, argomento
* 2019-03-01, intro corso, panoramica Sistemi Embedded


## Argomenti da evadere (non in ordine di presentazione)

FIXME verificare ci siano tutti

* terminologia, architetture
* Sistemi "monoprogrammati" e "multiprogrammati", MdT
* Conversione AD e DA
* Multiplexing
* Controlli automatici (PID)
* Modo di pensare
* Real Time
* Licenze Software
* Richiami di elettronica (principi)
* Forme d’onda
* Componenti di base
* PWM (Pulse Width Modulation)
* Semiconduttori
* Strumenti di misura
* Montaggio ﬁsico
* Sensori
* Attuatori
* MEMS
* Architetture Embedded (panoramica)
* Memorie, I/O e comunicazione
* I sistemi GNU/Linux (panoramica)
* FreeRTOS (cenni, forse)
* Arduino e Wiring
* Rete e protocolli (panoramica)
* Firmata
* OSC
* Motori, ponte H, etc.
* I2C
* TaskScheduler
* MQTT

# Sistemi Embedded 2018-2019

**Corso per laurea triennale**

A.A. 2018/2019 (SECONDO SEMESTRE)

  * [Programma ufficiale](http://www.ccdinfmi.unimi.it/it/corsiDiStudio/2018/F1Xof2/default/F1X-122/F1X-122.18.1/programma_it.html)
  * [Telegram](https://t.me/joinchat/ALnoPw8wxMVMet7DYU1NZQ)

Orari e aule:

* Lunedì, 11.30-13.30, aula Omega
* Venerdì, 11.30-13.30, aula Delta

(lezioni senza intervallo "inline", ma si comincia circa 10 min dopo orario ufficiale e si termina circa 10 min prima)

Modalità esame:

* orale/scritto (domande aperte)
* progetto da presentare in aula



## AVVISI

* INIZIO CORSO: venerdì 1/3



## Libro di testo

SISTEMI EMBEDDED: TEORIA E PRATICA

Alexjan Carraturo, Andrea Trentini

Seconda edizione

http://www.ledizioni.it/prodotto/a-carraturo-a-trentini-sistemi-embedded-teoria-pratica/

(si trova anche su Amazon scontato)


## Livello di conoscenza/approfondimento richiesto per ogni capitolo

FIXME nuova edizione

## Hardware consigliato

FIXME decidere aggiornamento piattaforma?

## URL delle board aggiuntive

FIXME aggiornare

Da mettere nel "board manager" dell'IDE (File -> Preferences -> Additional Boards Manager)

* http://digistump.com/package_digistump_index.json
* http://arduino.esp8266.com/stable/package_esp8266com_index.json
* http://raw.githubusercontent.com/esp8266/Arduino/master/boards.txt
* http://adafruit.github.io/arduino-board-index/package_adafruit_index.json
* https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json

## URL vari utili!

* [[https://github.com/esp8266/Arduino|Documentazione di riferimento per l'implementazione di Wiring su ESP8266]]
* http://www.nongnu.org/avr-libc/
* http://www.atmel.com/Images/Atmel-42735-8-bit-AVR-Microcontroller-ATmega328-328P_Datasheet.pdf
